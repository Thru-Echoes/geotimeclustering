import os 
from setuptools import setup, find_packages

## Utility function to show README 

def show_readme(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()

setup(
    name = "geotimeclustering",
    version = "0.0.1",
    author = "Oliver Muellerklein",
    author_email = "omuellerklein@berkeley.edu",
    description = ("Utility functions for data exploration, preprocessing, and ML"),
    packages = find_packages(),
    long_description = show_readme("README.md")
)
